package com.bulsa.practica.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.bulsa.practica.MyGdxGame;
import com.bulsa.practica.util.Constantes;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Constantes.ANCHURA;
		config.height = Constantes.ALTURA;
		config.fullscreen = false;
		new LwjglApplication(new MyGdxGame(), config);
	}

}
