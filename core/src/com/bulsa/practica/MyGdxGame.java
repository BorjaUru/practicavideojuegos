package com.bulsa.practica;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.bulsa.practica.screens.PantallaMenuPrincipal;

public class MyGdxGame extends Game {

	
	@Override
	public void create () {
		((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaMenuPrincipal());
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {

	}
}
