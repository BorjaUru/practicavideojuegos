package com.bulsa.practica.screens;


import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bulsa.practica.MyGdxGame;
import com.bulsa.practica.managers.ResourceManagers;
import com.bulsa.practica.util.Constantes;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;

/**
 * Created by borja on 21/02/2017.
 */
public class PantallaMenuPrincipal implements Screen {
    private MyGdxGame game;
    Stage stage;

    @Override
    public void show() {
        cargarpreferencias();
        ResourceManagers.cargarRecursos();
        if(!VisUI.isLoaded()){
            VisUI.load();
        }

        stage=new Stage();
        VisTable table=new VisTable();
        table.setPosition(Constantes.ANCHURA/2-table.getWidth()/2, Constantes.ALTURA/2);
        table.setFillParent(true);
        table.setHeight(500);
        stage.addActor(table);
        VisTextButton bIniciar=new VisTextButton("Jugar");
        bIniciar.setHeight(50);
        bIniciar.setWidth(200);
        bIniciar.setPosition(table.getWidth()/2-bIniciar.getWidth()/2,0);
        bIniciar.setColor(new Color(0,0,1,1));
        bIniciar.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaJuego("a.tmx",1));

                dispose();
            }
        });
        VisTextButton bConfiguracion=new VisTextButton("Configuracion");
        bConfiguracion.setHeight(50);
        bConfiguracion.setWidth(200);
        bConfiguracion.setPosition(table.getWidth()/2-bConfiguracion.getWidth()/2,-60);
        bConfiguracion.setColor(new Color(0,0,1,1));
        bConfiguracion.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaConfiguracion());
                dispose();
            }
        });
        VisTextButton bInstruc=new VisTextButton("Instrucciones");
        bInstruc.setHeight(50);
        bInstruc.setWidth(200);
        bInstruc.setPosition(table.getWidth()/2-bInstruc.getWidth()/2,-120);
        bInstruc.setColor(0,0,1,1);
        bInstruc.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaInstruciones());
                dispose();

            }
        });
        VisTextButton bExit=new VisTextButton("Salir");
        bExit.setHeight(50);
        bExit.setWidth(200);
        bExit.setPosition(table.getWidth()/2-bExit.getWidth()/2,-180);
        bExit.setColor(0,0,1,1);
        bExit.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                dispose();
                System.exit(0);
            }
        });
        table.addActor(bIniciar);
        table.addActor(bConfiguracion);
        table.addActor(bInstruc);
        table.addActor(bExit);
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }
    private void cargarpreferencias(){
        Preferences p= Gdx.app.getPreferences("holaligdx");

        boolean pantalla=p.getBoolean("completa");
        if(pantalla){
            Graphics.DisplayMode mode = Gdx.graphics.getDisplayMode();
            Gdx.graphics.setFullscreenMode(mode);
        }else{
            Gdx.graphics.setWindowedMode(Constantes.ANCHURA,Constantes.ALTURA);
        }
    }
    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();

    }
}
