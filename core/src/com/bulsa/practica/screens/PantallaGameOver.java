package com.bulsa.practica.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bulsa.practica.util.Constantes;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisImage;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;

/**
 * Created by borja on 27/02/2017.
 */
public class PantallaGameOver implements Screen {

    Stage stage;
    Texture a;
    @Override
    public void show() {
        if(!VisUI.isLoaded()){
            VisUI.load();}
        stage=new Stage();
        VisTable table=new VisTable();
        table.setPosition(Constantes.ANCHURA/2-table.getWidth()/2, Constantes.ALTURA/2);
        table.setFillParent(true);
        stage.addActor(table);
        VisImage iGameOver=new VisImage(a=new Texture("photo.png"));
        iGameOver.setPosition(table.getWidth()/2-iGameOver.getWidth()/2,0);
        iGameOver.setColor(new Color(0,0,1,1));

        VisTextButton bNueva=new VisTextButton("Nueva partida");
        bNueva.setHeight(50);
        bNueva.setWidth(200);
        bNueva.setPosition(table.getWidth()/2-bNueva.getWidth()/2,-60);
        bNueva.setColor(new Color(0,0,1,1));
        bNueva.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {


                ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaJuego("a.tmx",1));
                dispose();
            }
        });
        VisTextButton bConfigurar=new VisTextButton("Configuración");
        bConfigurar.setHeight(50);
        bConfigurar.setWidth(200);
        bConfigurar.setPosition(table.getWidth()/2-bConfigurar.getWidth()/2,-120);
        bConfigurar.setColor(0,0,1,1);
        bConfigurar.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaConfiguracion());

                dispose();
            }
        });
        VisTextButton bMenu=new VisTextButton("Inicio");
        bMenu.setHeight(50);
        bMenu.setWidth(200);
        bMenu.setPosition(table.getWidth()/2-bMenu.getWidth()/2,-180);
        bMenu.setColor(new Color(0,0,1,1));
        bMenu.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaMenuPrincipal());

                dispose();
            }
        });
        VisTextButton bSalir=new VisTextButton("Salir");
        bSalir.setHeight(50);
        bSalir.setWidth(200);
        bSalir.setPosition(table.getWidth()/2-bSalir.getWidth()/2,-240);
        bSalir.setColor(0,0,1,1);
        bSalir.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                dispose();
                System.exit(0);
            }
        });
        table.addActor(bNueva);
        table.addActor(bConfigurar);
        table.addActor(bMenu);
        table.addActor(bSalir);

        table.addActor(iGameOver);
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        a.dispose();
    }
}
