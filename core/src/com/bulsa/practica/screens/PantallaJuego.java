package com.bulsa.practica.screens;

import com.badlogic.gdx.*;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;

import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.bulsa.practica.managers.ResourceManagers;
import com.bulsa.practica.util.*;


/**
 * Created by borja on 27/02/2017.
 */
public class PantallaJuego implements Screen {
    Array<Objeto>objetos;
    Array<Enemigo>enemigos;
    Batch batch;
    BitmapFont fuente;
    TiledMap mapa;
    OrthogonalTiledMapRenderer mapRenderer;
    OrthographicCamera camara;
    Jugador j;
    String chocando;
    Bala ba;
    Music m;
    Boolean sonido;
    String level;
    int nivel;

    public PantallaJuego(String level, int nivel) {
        this.nivel=nivel;
        this.level=level;
        batch = new SpriteBatch();
        fuente = new BitmapFont(Gdx.files.internal("default.fnt"));
    }
    private void cargarpreferencias(){
        Preferences p= Gdx.app.getPreferences("holaligdx");
        sonido=p.getBoolean("sonido");

    }

    @Override
    public void show() {
        cargarpreferencias();
        m=ResourceManagers.obtenerMusica();

        if(sonido){

        m.isLooping();
        m.play();}

        chocando="n";
        objetos=new Array<Objeto>();
        enemigos=new Array<Enemigo>();
        j=new Jugador(0,800);
        camara = new OrthographicCamera();
        camara.setToOrtho(false, 25 * 64, 18 * 64);
        camara.update();

        mapa = new TmxMapLoader().load("levels/"+level);
        mapRenderer = new OrthogonalTiledMapRenderer(mapa);
        batch = mapRenderer.getBatch();

        mapRenderer.setView(camara);
        cargarMapa();
    }
    private void destruirBala(){
        ba=null;

    }
    @Override
    public void render(float dt) {

        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mapRenderer.render(new int[]{0, 1,2});
        fuente.setColor(0,0,0,1);
        batch.begin();
        if(ba!=null){
            ba.render(batch);
        }
        for (Objeto o:objetos) {
            o.render(batch);
        }
        for (Enemigo e:enemigos){
            if (!camara.frustum.pointInFrustum(new Vector3(e.posicion.x, e.posicion.y, 0)))
                continue;

            else {
                e.render(batch);
                String tipo=e.getClass().getSimpleName();
                if(tipo.equals("Mosca")){
                    Mosca m=((Mosca)e);

                    for (Bala b:m.balas) {

                        b.update();
                        b.render(batch);
                        if(TimeUtils.millis()-b.tiempo>2000){
                            m.balas.removeValue(b,true);
                        }
                        if (j.rect.overlaps(b.rect)){
                            m.balas.removeValue(b,true);
                            if(j.vida>0)
                                j.vida--;
                            else{
                                ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaGameOver());
                                dispose();
                            }
                        }
                    }
                    m.actualizar(dt);
                }else if(tipo.equals("Caracol")){
                    ((Caracol)e).actualizar(dt);
                }else if(tipo.equals("Slime")){
                    ((Slime)e).actualizar(dt);
                }
            }
        }
        j.render(batch);
        fuente.getData().setScale(2);
        j.update(dt);
        fuente.draw(batch,"Dinero : "+j.dinero,camara.position.x-camara.viewportWidth/2,camara.position.y*2-50);
        fuente.draw(batch,"Nivel : "+nivel,camara.position.x,camara.position.y*2-50);
        fuente.draw(batch,"Vidas : "+j.vida,camara.position.x+camara.viewportWidth/2-200,camara.position.y*2-50);
        batch.end();
        if(ba!=null){
        ba.update();}
        comprobarTeclado(dt);
        fijarCamara();
        comprobarColisiones(dt);
        colisionesEnemigos();
        colisionSlimeSuelo();
    }
    private void fijarCamara() {

        if (j.posicion.x < 25 * 64 / 2) {
            camara.position.set(25 * 64 / 2, 20 * 64 / 2 - 64, 0);
        }else if(j.posicion.x>(7000-(25*(64/2)))){
            camara.position.set(7000-(25*(64/2)),20 * 64 / 2 - 64, 0);
        }
        else {
            camara.position.set(j.posicion.x, 20 * 64 / 2 - 64, 0);
        }

        camara.update();
        mapRenderer.setView(camara);
    }
    private void comprobarColisiones(float dt) {
        boolean a=false;
        MapLayer capaColision = mapa.getLayers().get("colisiones");
        MapObjects mapObjects = capaColision.getObjects();
        for (MapObject mapObject : mapObjects) {
                Rectangle rect = ((RectangleMapObject) mapObject).getRectangle();
                if (rect.overlaps(j.rect)) {
                    String tipo = (String) mapObject.getProperties().get("s");
                    if(tipo.equals("s")){
                        //j.posicion.y = rect.y + rect.getHeight();
                        if(j.velocidad.y>0){

                         }else{
                         a=true;
                        j.tocando=true;
                        j.saltando = false;
                        }
                    }else if(tipo.equals("m")){
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaGameOver());
                        dispose();
                    }

                }
        }
        if(!a) {
            j.tocando = false;
        }
        boolean b=false;
        MapLayer capa = mapa.getLayers().get("colisionesPared");
        MapObjects ma = capa.getObjects();
        for (MapObject map : ma) {
            Rectangle rect = ((RectangleMapObject) map).getRectangle();
            if (rect.overlaps(j.rect)) {
                String tipo = (String) map.getProperties().get("lado");
                if(tipo.equals("d")){
                    chocando="d";
                }else if(tipo.equals("i")){
                    chocando="i";
                }
                b=true;
            }

        }
        if(!b){
            chocando="n";
        }
        for (Objeto o:objetos) {
            if(j.rect.overlaps(o.r)){
                if (o.getClass().getSimpleName().equals("Diamante")) {
                    Diamante d = (Diamante) o;
                    j.dinero+=d.a;
                    objetos.removeValue(o,true);
                } else if (o.getClass().getSimpleName().equals("Puerta")) {
                    if(j.dinero==40) {
                        if(nivel==2){
                            ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaMenuPrincipal());
                            dispose();
                        }else {
                            ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaJuego("aa.tmx", 2));
                            dispose();
                        }

                    }

                }else if (o.getClass().getSimpleName().equals("Muelle")) {
                    o.imagen=ResourceManagers.obtenerFrame("muellee");
                    j.velocidad.y =10;
                    j.update(dt);
                    ((Muelle)o).a=false;
                    final Muelle oo=(Muelle) o;
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            oo.imagen=ResourceManagers.obtenerFrame("muelle");
                            oo.a=true;

                        }
                    },2);
                }
            }/*
            if(o.getClass().getSimpleName().equals("Muelle")&&!((Muelle)o).a){
                ((Muelle)o).a=true;
                o.imagen=ResourceManagers.obtenerFrame("muelle");
            }*/
        }

    }
    private void comprobarTeclado(float dt) {

        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {

            if(chocando.equals("d")){
                j.velocidad.x=0;
            }else{
                j.velocidad.x = 150 * dt;
                j.estado = Jugador.Estado.DERECHA;
            }


        }
        else if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {

            if(chocando.equals("i") ){
                j.velocidad.x=0;
            }else{
                j.velocidad.x = -150 * dt;
                j.estado = Jugador.Estado.IZQUIERDA;
            }

        }
        else {
            j.estado = Jugador.Estado.QUIETO;
            j.velocidad.x = 0;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            if (!j.saltando) {

                j.posicion.y += 5;
                j.rect.y += 5;
                j.velocidad.y = 6;
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaMenuPrincipal());
            dispose();
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.CONTROL_LEFT)&&ba==null){
            Vector2 velocidad=null;
            if(j.estado.equals(Personaje.Estado.DERECHA)){
                velocidad=new Vector2(5,0);
            }else if(j.estado.equals(Personaje.Estado.IZQUIERDA)){
                velocidad=new Vector2(-5,0);
            }else{
                velocidad=new Vector2(5,0);
            }
            ba=new Bala(j.posicion.x+j.rect.getWidth()/2,j.posicion.y+j.rect.getHeight()/2,velocidad);
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    destruirBala();
                }
            },2);
            Preferences p= Gdx.app.getPreferences("holaligdx");
            boolean sonido=p.getBoolean("sonido");
            if(sonido) {
                Sound aa = ResourceManagers.obtenerSonido();
                aa.play();
            }
        }

    }
    private void colisionSlimeSuelo(){
        for(Enemigo e:enemigos){
            if(e.getClass().getSimpleName().equals("Slime")){
                MapLayer capaColision = mapa.getLayers().get("colisiones");
                MapObjects mapObjects = capaColision.getObjects();
                for (MapObject mapObject : mapObjects) {
                    Rectangle rect = ((RectangleMapObject) mapObject).getRectangle();
                    if (rect.overlaps(e.rect)) {
                        String tipo = (String) mapObject.getProperties().get("s");
                        if(tipo.equals("s")){
                            e.velocidad.y=0;
                            ((Slime)e).suelo=true;
                        }

                    }if(ba!=null) {
                        if (e.rect.overlaps(ba.rect)) {
                            ba = null;
                           matarEnemigo(e);
                        }
                    }
                }
            }
        }
    }
    private void colisionesEnemigos(){
        MapLayer capa = mapa.getLayers().get("colisionesPared");
        MapObjects ma = capa.getObjects();
        for (MapObject map : ma) {
            Rectangle rect = ((RectangleMapObject) map).getRectangle();
            for ( Enemigo enemigo :enemigos){
                if (rect.overlaps(enemigo.rect)) {
                    String tipo = (String) map.getProperties().get("lado");
                    if(tipo.equals("d")){
                        enemigo.velocidad.x=-4;
                        enemigo.estado= Enemigo.Estado.IZQUIERDA;
                    }else if(tipo.equals("i")){
                        enemigo.velocidad.x=4;
                        enemigo.estado= Enemigo.Estado.DERECHA;
                    }
                }if(enemigo.rect.overlaps(j.rect)){

                    if(enemigo.getClass().getSimpleName().equals("Slime")){
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaGameOver());
                        dispose();
                    }else if(enemigo.getClass().getSimpleName().equals("Mosca")){
                        matarEnemigo(enemigo);

                    }else if(enemigo.getClass().getSimpleName().equals("Caracol")){
                        if(j.saltando){
                            matarEnemigo(enemigo);
                        }else if(enemigo.estado.equals(Enemigo.Estado.MUERTOD)||enemigo.estado.equals(Enemigo.Estado.MUERTOI)){

                        }else {
                            ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaGameOver());
                            dispose();
                        }
                    }

                }
            }

        }

    }
    private void matarEnemigo(final Enemigo enemigo){
        if(enemigo.estado.equals(Enemigo.Estado.DERECHA)){
            enemigo.estado= Enemigo.Estado.MUERTOD;
        }else if (enemigo.estado.equals(Enemigo.Estado.IZQUIERDA)){
            enemigo.estado= Enemigo.Estado.MUERTOI;
        }
        enemigo.velocidad.x=0;
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                enemigos.removeValue(enemigo,true);
            }
        },2);
    }
    private void cargarMapa() {

        MapLayer capaObjetos = mapa.getLayers().get("objetos");
        MapObjects mapObjects = capaObjetos.getObjects();
        for (MapObject mapObject : mapObjects) {
            String tipo = (String) mapObject.getProperties().get("tipo");
            if (tipo.equals("diamante")) {
                String color = (String) mapObject.getProperties().get("color");
                TiledMapTileMapObject tileMapObject = (TiledMapTileMapObject) mapObject;
                TextureRegion imagen=null;
                int a=0;
               if(color.equals("rojo")){
                    imagen= ResourceManagers.obtenerFrame("gemRed");
                    a=20;
               }else if (color.equals("verde")){
                   imagen= ResourceManagers.obtenerFrame("gemGreen");
                   a=10;
               }else if(color.equals("azul")){
                   imagen= ResourceManagers.obtenerFrame("gemBlue");
                   a=5;
               }

                Diamante diamante = new Diamante(imagen,tileMapObject.getX(), tileMapObject.getY(), a);
                objetos.add(diamante);
            }else if(tipo.equals("muelle")){
                TiledMapTileMapObject tileMapObject = (TiledMapTileMapObject) mapObject;
                TextureRegion imagen=ResourceManagers.obtenerFrame("muelle");
                Muelle m=new Muelle(imagen,tileMapObject.getX(), tileMapObject.getY());
                objetos.add(m);
            }else if(tipo.equals("puerta")){
                String color = (String) mapObject.getProperties().get("seccion");
                TiledMapTileMapObject tileMapObject = (TiledMapTileMapObject) mapObject;
                TextureRegion imagen=null;
                if(color.equals("a")){
                    imagen=ResourceManagers.obtenerFrame("doorA");
                }else if(color.equals("b")){
                    imagen=ResourceManagers.obtenerFrame("doorB");
                }
                Puerta p=new Puerta(imagen,tileMapObject.getX(), tileMapObject.getY());
                objetos.add(p);
            }
        }
        MapLayer capaObjetoss = mapa.getLayers().get("enemigos");
        MapObjects mapObjectss = capaObjetoss.getObjects();
        for (MapObject mapObjec : mapObjectss) {
            String tipo = (String) mapObjec.getProperties().get("tipo");
            if (tipo.equals("caracol")) {
                TiledMapTileMapObject tileMapObject = (TiledMapTileMapObject) mapObjec;
                Caracol c = new Caracol(tileMapObject.getX(), tileMapObject.getY());
                enemigos.add(c);

            }else if (tipo.equals("slime")) {
                TiledMapTileMapObject tileMapObject = (TiledMapTileMapObject) mapObjec;
                Slime c = new Slime(tileMapObject.getX(), tileMapObject.getY());
                enemigos.add(c);

            }else if (tipo.equals("mosca")) {
                TiledMapTileMapObject tileMapObject = (TiledMapTileMapObject) mapObjec;
                Mosca c = new Mosca(tileMapObject.getX(), tileMapObject.getY());
                enemigos.add(c);

            }
        }
    }
    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

        fuente.dispose();
        m.stop();
    }

}
