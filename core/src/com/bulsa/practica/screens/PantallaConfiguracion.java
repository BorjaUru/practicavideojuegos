package com.bulsa.practica.screens;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bulsa.practica.MyGdxGame;
import com.bulsa.practica.util.Constantes;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.*;

/**
 * Created by borja on 27/02/2017.
 */
public class PantallaConfiguracion implements Screen {

    Stage stage;
    private VisCheckBox cbSonido,cbPantallaCompleta;

    private void cargarpreferencias(){
        Preferences p= Gdx.app.getPreferences("holaligdx");
        cbSonido.setChecked(p.getBoolean("sonido"));
        cbPantallaCompleta.setChecked(p.getBoolean("completa"));
    }

    private void guardarPreferencencias(){
        Preferences p=Gdx.app.getPreferences("holaligdx");
        p.putBoolean("sonido",cbSonido.isChecked());
        p.putBoolean("completa",cbPantallaCompleta.isChecked());

        p.flush();
    }

    @Override
    public void show() {
        if(!VisUI.isLoaded()){
            VisUI.load();}
        stage=new Stage();
        VisTable table=new VisTable();

        table.setPosition(Constantes.ANCHURA/2-table.getWidth()/2,Constantes.ALTURA/2);
        table.setFillParent(true);

        stage.addActor(table);
        cbSonido=new VisCheckBox("Sonido");
        cbSonido.setHeight(50);
        cbSonido.setWidth(200);
        cbSonido.setPosition(table.getWidth()/2-cbSonido.getWidth()/2,0);
        cbSonido.setColor(new Color(0,0,1,1));
        cbSonido.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

            }
        });


        cbPantallaCompleta=new VisCheckBox("Pantalla completa");
        cbPantallaCompleta.setHeight(50);
        cbPantallaCompleta.setWidth(200);
        cbPantallaCompleta.setPosition(table.getWidth()/2-cbPantallaCompleta.getWidth()/2,-60);
        cbPantallaCompleta.setColor(new Color(0,0,1,1));
        cbPantallaCompleta.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

            }
        });
        VisTextButton bHecho=new VisTextButton("Hecho");
        bHecho.setHeight(50);
        bHecho.setWidth(200);
        bHecho.setPosition(table.getWidth()/2-bHecho.getWidth()/2,-240);
        bHecho.setColor(0,0,1,1);
        bHecho.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                guardarPreferencencias();
                if(cbPantallaCompleta.isChecked()){
                    Graphics.DisplayMode mode = Gdx.graphics.getDisplayMode();
                    Gdx.graphics.setFullscreenMode(mode);
                }else{
                    Gdx.graphics.setWindowedMode(Constantes.ANCHURA,Constantes.ALTURA);
                }
                ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaMenuPrincipal());
                dispose();
            }
        });
        cargarpreferencias();
        table.addActor(cbSonido);
        table.addActor(cbPantallaCompleta);
        table.addActor(bHecho);
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
