package com.bulsa.practica.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bulsa.practica.MyGdxGame;
import com.bulsa.practica.util.Constantes;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisImage;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;

/**
 * Created by borja on 28/02/2017.
 */
public class PantallaInstruciones implements Screen {
    private MyGdxGame game;
    Stage stage;
    Texture a;
    @Override
    public void show() {
        if(!VisUI.isLoaded()){
            VisUI.load();
        }
        stage=new Stage();
        VisTable table=new VisTable();
        table.setPosition(Constantes.ANCHURA/2-table.getWidth()/2, Constantes.ALTURA/2);
        table.setFillParent(true);
        table.setHeight(500);
        stage.addActor(table);
        VisImage iGameOver=new VisImage(a=new Texture("instruciones.png"));
        iGameOver.setPosition(table.getWidth()/2-iGameOver.getWidth()/2,0);
        iGameOver.setColor(new Color(0,0,1,1));
        VisTextButton bHecho=new VisTextButton("Hecho");
        bHecho.setHeight(50);
        bHecho.setWidth(200);
        bHecho.setPosition(table.getWidth()/2-bHecho.getWidth()/2,-240);
        bHecho.setColor(0,0,1,1);
        bHecho.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                ((Game) Gdx.app.getApplicationListener()).setScreen(new PantallaMenuPrincipal());
                dispose();
            }
        });

        table.addActor(bHecho);
        table.addActor(iGameOver);
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
