package com.bulsa.practica.util;


import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;


import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.bulsa.practica.managers.ResourceManagers;

/**
 * Created by borja on 07/03/2017.
 */
public class Bala {
    public long tiempo;
    TextureRegion imagen;
    Vector2 posicion;
    public Rectangle rect;
    Vector2 velocidad;
    public Bala(float x,float y,Vector2 velocidad){
        this.imagen= ResourceManagers.obtenerFrame("bullet");
        this.posicion=new Vector2(x, y);
        this.rect =new Rectangle(x,y,imagen.getRegionWidth(),imagen.getRegionHeight());
        this.velocidad=velocidad;
        tiempo= TimeUtils.millis();
    }
    public void render(Batch batch) {
        batch.draw(imagen, posicion.x, posicion.y);
    }
    public void update(){

        posicion.add(velocidad);
        rect.setPosition(posicion);
    }
}
