package com.bulsa.practica.util;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by borja on 28/02/2017.
 */
public abstract class Objeto {
    public TextureRegion imagen;
    public Rectangle r;

    public Objeto(TextureRegion imagen,float x,float y){
        this.imagen=imagen;

        r=new Rectangle(x,y,imagen.getRegionWidth(),imagen.getRegionHeight());
    }
    public void dispose(){

    }
    public void render(Batch batch) {
        batch.draw(imagen, r.x, r.y);
    }
}
