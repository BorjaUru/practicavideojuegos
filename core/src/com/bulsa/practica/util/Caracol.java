package com.bulsa.practica.util;

/**
 * Created by borja on 07/03/2017.
 */
public class Caracol extends Enemigo {
    public Caracol(float x, float y) {
        super(x, y, "snailD", "snailI","snailDM","snailIM");
        super.velocidad.x=4;
    }
    public void actualizar(float dt){
        super.update(dt);

    }
}
