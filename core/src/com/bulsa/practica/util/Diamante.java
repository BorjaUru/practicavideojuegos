package com.bulsa.practica.util;


import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by borja on 28/02/2017.
 */
public class Diamante extends Objeto{
    public int a;
    public Diamante(TextureRegion imagen, float x, float y,int a) {
        super(imagen, x, y);
        this.a=a;
    }

}
