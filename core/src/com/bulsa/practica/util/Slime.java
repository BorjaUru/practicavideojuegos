package com.bulsa.practica.util;


import com.badlogic.gdx.utils.Timer;

/**
 * Created by borja on 07/03/2017.
 */
public class Slime extends Enemigo {
   public boolean suelo;
    public Slime(float x, float y) {
        super(x, y, "slimeD", "slimeI","slimeDeadD","slimeDeadI");
        super.velocidad.x=4;
        suelo=true;

    }
    public void actualizar(float dt){
        super.update(dt);
        if(suelo){
            super.posicion.y+=2;
            super.velocidad.y=1;
            suelo=false;
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    caer();
                }
            },1);
        }
    }
    public void caer(){
       super.velocidad.y=-4;
    }

}
