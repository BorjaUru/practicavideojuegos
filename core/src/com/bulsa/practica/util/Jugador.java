package com.bulsa.practica.util;

/**
 * Created by borja on 28/02/2017.
 */
public class Jugador extends Personaje {
    public int dinero;
    public int vida;

    public Jugador(float x, float y) {
        super(x, y, "p1_walk", "p2_walk","p1_front");
        dinero=0;
        vida=1000;
    }
}
