package com.bulsa.practica.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.bulsa.practica.managers.ResourceManagers;

/**
 * Created by borja on 07/03/2017.
 */
public class Mosca extends Enemigo {
    public Array<Bala> balas;
    long tiempoR;
    float posicionIni;
    public Mosca(float x, float y) {
        super(x, y, "moscaD", "moscaI","moscaD_muerta","moscaI_muerta");
        super.velocidad.x=3;
        posicionIni=x;
        balas=new Array<Bala>();
        tiempoR= TimeUtils.millis();
    }
    public void actualizar(float dt){
        super.update(dt);
        if(Math.abs(posicionIni-super.posicion.x)>100){
            if (super.estado.equals(Estado.DERECHA)){
                super.estado=Estado.IZQUIERDA;
                super.velocidad.x=-3;
            }else if(super.estado.equals(Estado.IZQUIERDA)){
                super.estado=Estado.DERECHA;
                super.velocidad.x=3;
            }
        }
        boolean a=true;
        if(super.estado.equals(Estado.MUERTOD)||super.estado.equals(Estado.MUERTOI)){
            a=false;
        }
        if (TimeUtils.millis()-tiempoR>1000&&a){
            Vector2 velocidad=null;
            if(super.estado.equals(Estado.DERECHA)){
                velocidad=new Vector2(3,-3);
            }else if(super.estado.equals(Estado.IZQUIERDA)){
                velocidad=new Vector2(-3,-3);
            }
            balas.add(new Bala(super.posicion.x,super.posicion.y,velocidad));
            tiempoR=TimeUtils.millis();
            Preferences p= Gdx.app.getPreferences("holaligdx");
            boolean sonido=p.getBoolean("sonido");
            if(sonido) {
                Sound aa = ResourceManagers.obtenerSonido();
                aa.play();
            }

        }


    }
}
