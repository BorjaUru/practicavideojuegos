package com.bulsa.practica.util;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.bulsa.practica.managers.ResourceManagers;

/**
 * Created by borja on 07/03/2017.
 */
public abstract class Enemigo {
    protected TextureRegion frameActual;
    protected Animation animacionDerecha;
    protected Animation animacionIzquierda;
    protected String muertoD;
    protected String muertoI;
    public Vector2 posicion;
    public Vector2 velocidad;
    public Rectangle rect;
    private float tiempo;
    public Estado estado;
    public enum Estado {
        DERECHA, IZQUIERDA,MUERTOD,MUERTOI;
    }
    public Enemigo(float x, float y, String nombreAnimacionDerecha,
                     String nombreAnimacionIzquierda,String muertoD,String muertoI) {

        posicion = new Vector2(x, y);
        velocidad = new Vector2(0, 0);

        animacionDerecha = new Animation(0.25f, ResourceManagers.obtenerAnimacion(nombreAnimacionDerecha));
        animacionIzquierda = new Animation(0.25f,
                ResourceManagers.obtenerAnimacion(nombreAnimacionIzquierda));

        estado = Estado.DERECHA;
        tiempo = 0;
        this.muertoD=muertoD;
        this.muertoI=muertoI;
        frameActual = (TextureRegion) animacionDerecha.getKeyFrame(0);
        rect = new Rectangle(posicion.x, posicion.y,
                frameActual.getRegionWidth(), frameActual.getRegionHeight());

    }

    public void update(float dt) {

        tiempo += dt;

        switch (estado) {
            case DERECHA:
                frameActual = (TextureRegion) animacionDerecha.getKeyFrame(tiempo, true);
                break;
            case IZQUIERDA:
                frameActual = (TextureRegion) animacionIzquierda.getKeyFrame(tiempo, true);
                break;
            case MUERTOD:
                frameActual=ResourceManagers.obtenerFrame(muertoD);
                break;
            case MUERTOI:
                frameActual=ResourceManagers.obtenerFrame(muertoI);
                break;
            default:
        }
        posicion.add(velocidad);
        rect.setPosition(posicion);


    }

    public void render(Batch batch) {
        batch.draw(frameActual, posicion.x, posicion.y);
    }

    public void dispose() {
    }
}
