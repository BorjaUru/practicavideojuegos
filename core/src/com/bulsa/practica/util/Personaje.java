package com.bulsa.practica.util;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.bulsa.practica.managers.ResourceManagers;

/**
 * Created by borja on 28/02/2017.
 */
public abstract class Personaje {
    protected TextureRegion frameActual;
    protected Animation animacionDerecha;
    protected Animation animacionIzquierda;

    public Vector2 posicion;
    public Vector2 velocidad;
    public Rectangle rect;
    private float tiempo;
    public Estado estado;
    public boolean saltando;
    public boolean tocando;
    String front;
    public enum Estado {
        DERECHA, IZQUIERDA, QUIETO;
    }

    public Personaje(float x, float y, String nombreAnimacionDerecha,
                     String nombreAnimacionIzquierda,String front) {

        posicion = new Vector2(x, y);
        velocidad = new Vector2(0, 0);
        tocando=false;
        animacionDerecha = new Animation(0.25f, ResourceManagers.obtenerAnimacion(nombreAnimacionDerecha));
        animacionIzquierda = new Animation(0.25f,
                ResourceManagers.obtenerAnimacion(nombreAnimacionIzquierda));

        estado = Estado.QUIETO;
        tiempo = 0;
        saltando = true;

        frameActual = (TextureRegion) animacionDerecha.getKeyFrame(0);
        rect = new Rectangle(posicion.x, posicion.y,
                frameActual.getRegionWidth(), frameActual.getRegionHeight());
        this.front=front;
    }

    public void update(float dt) {

        tiempo += dt;

        switch (estado) {
            case DERECHA:
                frameActual = (TextureRegion) animacionDerecha.getKeyFrame(tiempo, true);
                break;
            case IZQUIERDA:
                frameActual = (TextureRegion) animacionIzquierda.getKeyFrame(tiempo, true);
                break;
            case QUIETO:
                frameActual = ResourceManagers.obtenerFrame(front);
                break;
            default:
        }
    if(!tocando) {
        velocidad.y -= 10 * dt;
        if (velocidad.y < -10)
            velocidad.y = -10;
    }else if(!saltando){
            velocidad.y=0;
    }
        posicion.add(velocidad);
        rect.setPosition(posicion);

        if (velocidad.y > 1)
            saltando = true;

    }

    public void render(Batch batch) {
        batch.draw(frameActual, posicion.x, posicion.y);
    }

    public void dispose() {
    }

}
