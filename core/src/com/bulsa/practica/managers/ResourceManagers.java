package com.bulsa.practica.managers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * Created by borja on 21/02/2017.
 */
public class ResourceManagers {
    private static AssetManager assetManager = new AssetManager();

    /**
     * Carga todos los assets del juego
     */
    public static void cargarRecursos() {

        assetManager.load("atlas.pack", TextureAtlas.class);
        assetManager.load("levels/sonidos/a.mp3", Music.class);
        assetManager.load("levels/sonidos/o.mp3",Sound.class);
        assetManager.finishLoading();

        //assetManager.load("explosion.wav", Sound.class);

    }

    /**
     * Obtiene los frame de una animación
     * @param nombre
     * @return
     */
    public static Array<TextureAtlas.AtlasRegion> obtenerAnimacion(String nombre) {

        return assetManager.get("atlas.pack", TextureAtlas.class)
                .findRegions(nombre);
    }

    /**
     * Obtiene el primer frame de una animación
     * @param nombre
     * @return
     */
    public static TextureRegion obtenerFrame(String nombre) {

        return assetManager.get("atlas.pack", TextureAtlas.class)
                .findRegion(nombre);
    }

    public static Sound obtenerSonido() {

        return assetManager.get("levels/sonidos/o.mp3",Sound.class);
    }
    public static Music obtenerMusica(){
        return assetManager.get("levels/sonidos/a.mp3",Music.class);
    }
}
